//const customOrder = require('./lib/order')
module.exports = function(bp) {
  // Listens for a first message (this is a Regex)
  // GET_STARTED is the first message you get on Facebook Messenger
  bp.hear(['hi', 'Hi', 'Hello','hello','GET_STARTED', 'What can you do?','what can you do'], (event, next) => {
    event.reply('#greeting');
    const todos = [{
      title: "Perform an action", susbtitle: "Click to perform avsailable options", image_url: "https://img.buzzfeed.com/buzzfeed-static/static/2013-11/enhanced/webdr03/18/17/enhanced-buzz-22126-1384812593-32.jpg?crop=556:415;37,0&downsize=715:*&output-format=auto&output-quality=auto"
    }, {
      title: "Commonly bought meals", number: 1, subtitle: "View the most bought meals"
      }, {
        title: "Custom Order", number: 2, subtitle: "Make a custom order"
      }, {
        title: "View Food Categories", number: 3,
      }
  ];
  
  event.reply("#todoListTemplate", {actionList: todos})
   // event.reply('quickReply') // See the file `content.yml` to see the block
  });

  //we want to listen for our postback here. When user
  //clicks on Explore button
  bp.hear(/jddd$/i, (event, next) => {
    let txt = txt => bp.messenger.sendText(event.user.id, txt);
    bp.messenger.sendText(event.user.id, "Right. I guess I am just going to show you some indian foods")
    const foods = [{
      name: "Biryani", number: 1, description: "An aromatic rice dish cooked with several spices, notably saffron, and a protein (typically chicken or mutton) that's been marinated",image_url:"https://img.buzzfeed.com/buzzfeed-static/static/2013-11/enhanced/webdr03/18/17/enhanced-buzz-22126-1384812593-32.jpg?crop=556:415;37,0&downsize=715:*&output-format=auto&output-quality=auto"
    },{
      name: "Momos", number: 2, description: "A variation on the traditional dimsum, native to the North Eastern states that border Nepal (where the dish originated), eaten with a fiery red chutney.", image_url: "https://img.buzzfeed.com/buzzfeed-static/static/2013-11/enhanced/webdr07/18/17/enhanced-buzz-6623-1384814755-21.jpg?downsize=715:*&output-format=auto&output-quality=auto"
    }, {
      name: "Idli", number: 3, description:" A heavy South Indian breakfast food. A fermented batter of ground rice and lentils, steamed in little circular moulds.", image_url: "https://img.buzzfeed.com/buzzfeed-static/static/2013-11/enhanced/webdr03/18/17/enhanced-buzz-23071-1384814185-36.jpg?crop=600:447;0,5&downsize=715:*&output-format=auto&output-quality=auto"
    }, {
      name: "Chole Bhature", number: 4, description: " A spiced, curried chickpea dish served with a fried flour bread.", image_url:"https://img.buzzfeed.com/buzzfeed-static/static/2013-11/enhanced/webdr05/18/17/enhanced-buzz-orig-7491-1384813081-10.jpg?downsize=715:*&output-format=auto&output-quality=auto"
    }, {
      name: "Gulab jaamun", number:5, description:"Small balls of dried milk, slow cooked and boiled in a sugar syrup.", image_url:"https://img.buzzfeed.com/buzzfeed-static/static/2013-11/enhanced/webdr06/18/17/enhanced-buzz-6223-1384813996-35.jpg?downsize=715:*&output-format=auto&output-quality=auto"
    }
  ]

  
  // we create an array object, then we input the object properties : name, description, etc
  // this simply means what we are going to send back to the carousel bloc in our content.yml
  // read the facebook generic template to understand more
  //https://developers.facebook.com/docs/messenger-platform/send-messages/template/generic
  //event.reply('#indianFoodsCarousel', {foodResults: foods})
  // when we use event.reply('#blocName), we can pass data the bloc we are calling
  //in this case, we are calling indianFoodsCarousel and we want to display data
  // remember in our UMM, we are actually looping through the carousel object
  //this is why we have: {{#foodsResults}}......{{/foods}}
  // read more here: https://botpress.io/docs/foundamentals/umm.html
  });

  bp.hear(/(\d+)_PAYLOAD/i, (event, reply)=>{
    let txtt = txt => bp.messenger.sendText(event.user.id, txt);
    const categoryCarousel = [{
      name: "Small chops", number:1, description:"Small chops for refreshment", image_url: "https://s3.amazonaws.com/www.dealdey.com/system/deals/images/50003/S670x414/S-D-Product-Itnar-Events-LS1233.jpg?1419347302"
    }, {
      name: "Fruits", number:2, description: "Fresh fruits for health", image_url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ3Iucaa9s09KwP6oZcacngkb4YTgocQwUO4prue5vzUCYhLT2S4g"
    }, {
      name: "Fries", number: 3, description: "Tasty fried treats", image_url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQeKaNenicKMkPr-Q6aag4qli_CvP4R5q5Mm98ZLCDcNHra1g-G"
    }, {
      name: "Roasts", numbers: 4, description: "Delicious meat and fish roasts", image_url: "https://www.bbcgoodfood.com/sites/default/files/styles/recipe/public/recipe_images/recipe-image-legacy-id--2285_11.jpg?itok=9ojOBPoJ"
    }, {
      name: "Vegetable side", number: 5, description: "Well prepared fresh vegetables to supplement your meal", image_url: "http://cdn-image.myrecipes.com/sites/default/files/image/recipes/rs/2003/broccoli-lemon-rs-0524250-x.jpg"
    }, {
      name: "Soup", number: 6, description: "Tasty soups made with meat", image_url: "http://food.fnr.sndimg.com/content/dam/images/food/fullset/2016/2/26/1/WU1303H_Mexican-Chicken-Soup_s4x3.jpg.rend.hgtvcom.616.462.suffix/1459542226801.jpeg"
    }, {
      name: "Bread & Dough", number: 7, description: "Wheat and Non-wheat bread and Dough", image_url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR7sQ_XmoywezepJ1UonLp84bPj7IyIoax5Fr2wUzlMRuBfIuqI"
    }, {
      name: "Confectionary", number: 8, description: "Sweet confectionaries ranging from Cupcakes to Muffins", image_url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSdpQEzdqKW1lQ4zRp_pd-QEwDcbKa71ryvKaROYmY-kDyAw2t0"
    }, {
      name: "Pasta", number: 9, description: "Delicious Italian pastas", image_url: "https://assets.epicurious.com/photos/55f72d733c346243461d496e/master/pass/09112015_15minute_pastasauce_tomato.jpg"
    }, {
      name: "Salad", number: 10, description: "Well prepared salads", image_url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRxZMr7FwFmdLFAtGpvi_61UQJuGiCNAgMckyY3Q3YpJ9LcFCgNpg"
    }]







    let txt = txt => bp.messenger.sendText(event.user.id, txt);
    let rex = event.text.slice(0);
    let number = parseInt(rex.slice(0));
    console.log("LIST CATEGORY PAYLOAD === ", number);

    const foods = [{
      name: "Biryani", number: 1, description: "An aromatic rice dish cooked with several spices, notably saffron, and a protein (typically chicken or mutton) that's been marinated",image_url:"https://img.buzzfeed.com/buzzfeed-static/static/2013-11/enhanced/webdr03/18/17/enhanced-buzz-22126-1384812593-32.jpg?crop=556:415;37,0&downsize=715:*&output-format=auto&output-quality=auto"
    },{
      name: "Momos", number: 2, description: "A variation on the traditional dimsum, native to the North Eastern states that border Nepal (where the dish originated), eaten with a fiery red chutney.", image_url: "https://img.buzzfeed.com/buzzfeed-static/static/2013-11/enhanced/webdr07/18/17/enhanced-buzz-6623-1384814755-21.jpg?downsize=715:*&output-format=auto&output-quality=auto"
    }, {
      name: "Idli", number: 3, description:" A heavy South Indian breakfast food. A fermented batter of ground rice and lentils, steamed in little circular moulds.", image_url: "https://img.buzzfeed.com/buzzfeed-static/static/2013-11/enhanced/webdr03/18/17/enhanced-buzz-23071-1384814185-36.jpg?crop=600:447;0,5&downsize=715:*&output-format=auto&output-quality=auto"
    }, {
      name: "Chole Bhature", number: 4, description: " A spiced, curried chickpea dish served with a fried flour bread.", image_url:"https://img.buzzfeed.com/buzzfeed-static/static/2013-11/enhanced/webdr05/18/17/enhanced-buzz-orig-7491-1384813081-10.jpg?downsize=715:*&output-format=auto&output-quality=auto"
    }, {
      name: "Gulab jaamun", number:5, description:"Small balls of dried milk, slow cooked and boiled in a sugar syrup.", image_url:"https://img.buzzfeed.com/buzzfeed-static/static/2013-11/enhanced/webdr06/18/17/enhanced-buzz-6223-1384813996-35.jpg?downsize=715:*&output-format=auto&output-quality=auto"
    }
  ]

    switch(number) {

      case 1: txt("Oh!! You want to see the common meals we have. Here you go 🏃‍🏃‍"); event.reply('#indianFoodsCarousel', {foodResults: foods});event.reply('#initB')
      break;

      case 2: txt("Alright!! Let's get started 📝, shall we? 📔✍ 📔✍");

      bp.convo.start(event, convo => {
        if (bp.convo.find(event)) {
          return event.reply('#stopPreviousConvo')
        }
        convo.threads['default'].addQuestion("#mainCourse", response => {
            let mainCourse = response.text;
            convo.set('mainCourse', mainCourse);
            convo.switchTo('dessert');
        });

        convo.createThread('dessert');
        convo.threads['dessert'].addQuestion("#dessert", response=>{
            let dessert = response.text;
            convo.set('dessert', dessert);
            convo.switchTo('drinks');
        });

        convo.createThread('drinks');
        convo.threads['drinks'].addQuestion("#drinks", response =>{
            let drinks = response.text
            convo.set('drinks', drinks);
            convo.switchTo('total')
        });

        convo.createThread('total');
        convo.threads['total'].addQuestion("#totalBudget", [
            {
                pattern: /(\d+)/,
                callback: response => {
                    let totalPrice = response.match;
                    totalPrice < 5000 ?
                    (convo.say(txt("Your budget is smaller than the minimum. Enter a higher value")) ,convo.repeat())
                    : (convo.set('total', response.match),convo.say(txt("Okay, I got everything. Attached is the summary")), convo.next());
                    convo.next();
                }
            }, {
                default: true,
                callback: response => {
                    convo.say(txt("Ooops!! Doesn't look like a regular number. Avoid using text in your input. 5k is not the same as 5000."));
                    convo.repeat();
                }
            }
        ]);

        convo.on('done', () => {
            let total = parseInt(convo.get('total'), 10);
            let drinks = convo.get('drinks');
            let dessert = convo.get('dessert');
            let mainCourse = convo.get('mainCourse');
            txt(`All is set.
Your order is:
Main course: *${mainCourse}*.
Dessert: *${dessert}*.
Drinks: *${drinks}*.
Your budget is: *₹${total}*.
I have sent your receipt and summary to your email address. Thank you for ordering. I hope to see you again
`);
event.reply('#initB');
        });
    }); 

    
       break;

      case 3: txt("Here are the available categories we have");
      event.reply("#indianFoodsCarousel", {foodResults: categoryCarousel});
      event.reply('#initB')
       break;
    }
  })

bp.hear('INIT_YES', (event, reply) => {
  bp.messenger.sendText(event.user.id, "Alright! Let's get you started");
  const todos = [{
    title: "Perform an action", susbtitle: "Click to perform avsailable options", image_url: "https://img.buzzfeed.com/buzzfeed-static/static/2013-11/enhanced/webdr03/18/17/enhanced-buzz-22126-1384812593-32.jpg?crop=556:415;37,0&downsize=715:*&output-format=auto&output-quality=auto"
  }, {
    title: "Commonly bought meals", number: 1, subtitle: "View the most bought meals"
    }, {
      title: "Custom Order", number: 2, subtitle: "Make a custom order"
    }, {
      title: "View Food Categories", number: 3,
    }
];

event.reply("#todoListTemplate", {actionList: todos})
});

bp.hear('INIT_NO', (event, next) => {
  let first_name = event.user.first_name;
  bp.messenger.sendText(event.user.id, `Alright, ${first_name}. I hope to see you another time. Remain well fed!`)
});


  
  bp.hear(/select_food_(.+)/i, (event, next) => {
    let txt = txt => bp.messenger.sendText(event.user.id, txt);
    let rex = event.text.slice(-1)
    let number = parseInt(rex.slice(-1))
    //const txt = txt => bp.messenger.sendText(event.user.id, txt);
    console.log(number);
    switch(number) {
      case 1:
      txt("You want Bryanni");
      event.reply('quickReply')
      break;
      case 2: 
      txt("You want Mamos");
      event.reply('quickReply')
      break;
      case 3:
      txt("You want Idli");
      event.reply('quickReply')
      break;
      case 4:
      txt("You want Chole Bhature");
      event.reply('quickReply')
      break;
      case 5: txt("You want Gulab Jaamun");
      event.reply('quickReply')
      break;
    }
  });

//here, we want to ask our user questions and the answer
//we get would depend on the user's previous answer
//therefore, we use convo

bp.hear(/quickReply.b1/i, (event, next) => {
  let txt = txt => bp.messenger.sendText(event.user.id, txt);
  if (bp.convo.find(event)) {
    return event.reply('#stoppreviousConvo')
  }
  bp.convo.start(event, convo => {
    convo.threads['default'].addQuestion(txt("What is your name?", response => {
      convo.set('username', response.text);
      convo.switchTo('phgoneNumber')
    }))
    convo.createThread('phoneNumber');
    convo.threads['phoneNumber'].addQuestion(txt("WHat is your phone number?", [
      {
        pattern: /(\d+)/,
        callback: (reponse => { 
          convo.set('number', reponse.match);
        })
      }, {
        default: true,
        callback: reponse => {
          convo.say(txt("That doesn't look like a number. Please try again"));
          convo.repeat()
        }
      }
    ]))
    convo.on('done', () => {
      let username = convo.get('username');
      let phone = convo.get('phoneNumber');
      txt(`Your Username is: ${username} and phone number is: ${phone}`)
    })
  })
})



// if there was a prvious convo, we want to
//cancel it when a user clicks on the stop convo button

bp.hear('sdjkfkj', (event, next) => {
  let txt = txt => bp.messenger.sendText(event.user.id, txt);
  const convo = bp.convo.find(event) 
  if (convo) {
    convo.stop('aborted')
  }
  txt("I have cleared our previous convo");
  event.reply('#quickReply');
});

  // You can also pass a matcher object to better filter events
  bp.hear({
    type: /message|text/i,
    text: /exit|bye|goodbye|quit|done|leave|stop/i
  }, (event, next) => {
    event.reply('#goodbye', {
      // You can pass data to the UMM bloc!
      reason: 'unknown'
    });
  });
};
