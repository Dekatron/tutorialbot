module.exports = (bp) =>{
    bp.hear(' ', (event, reply) => {
        bp.convo.start(event, convo => {
            let txt = txt => bp.messenger.sendText(event.user.id, txt);
            convo.threads['default'].addQuestion(txt('What is should the main course be?', response => {
                let mainCourse = response.text;
                convo.set('mainCourse', mainCourse);
                convo.switchTo('dessert');
            }));
    
            convo.createThread('dessert');
            convo.threads['dessert'].addQuestion(txt("What dessert do you want?", response=>{
                let dessert = response.text;
                convo.set('dessert', dessert);
                convo.switchTo('drinks');
            }));
    
            convo.threads('drinks');
            convo.threads['drinks'].addQuestion(txt("What drink do you want? I recommend pepsi", response =>{
                let drinks = response.text
                convo.set('drinks', drinks);
                convo.switchTo('total')
            }));
    
            convo.threads('total');
            convo.threads['total'].addQuestion(txt("I got your preferences. I need to know the total amount you are willing to pay. Please note the smallest is #5000", [
                {
                    pattern: /(\d+)/,
                    callback: response => {
                        let totalPrice = response.match;
                        totalPrice < 5000 ?
                        (convo.say(txt("Your budget is smaller than the minimum. Enter a higher value")) ,convo.repeat())
                        : (convo.set('total', response.match),"Okay, I got that!", convo.next());
                    }
                }, {
                    default: true,
                    callback: response => {
                        convo.say(txt("Ooops!! Doesn't look like a regular number. Avoid using text in your input. 5k is not the same as 5000."));
                        convo.repeat();
                    }
                }
            ]));

            
            convo.on('done', () => {
                let total = parseInt(convo.get('total'), 10);
                let drinks = convo.get('drinks');
                let dessert = convo.get('dessert');
                let mainCourse = convo.get('mainCourse');
                txt(`
                All is set. Your order is:
                Main course: ${mainCourse}.
                Dessert: ${dessert}.
                Drinks: ${drinks}.
                Your budget is: #${total}
                `);
            });
        });   
    });
    
};